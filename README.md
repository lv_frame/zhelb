# 浙里办对接模块使用手册

## 1. 浙里办法人登录

### 1.1 使用举例
必要条件：

- 请浙里办技术支持在测试环境中添加回调地址白名单，如：
  ```shell
  老师，测试环境帮我们配置下回调地址吧
  access_key:5319d9...71fd4
  测试回调地址：https://test.com/api/zhelib/login
  ```
- url：用于请求浙里办的接口调用地址  
  政务外网环境接口地址：
  https://bcdsg.zj.gov.cn:8443/.../rest/user/query  
  互联网环境接口地址：
  https://ibcdsg.zj.gov.cn:8443/.../rest/user/query  
- access_key：组件超市进行组件使用申请； 
- secret_key：组件超市进行组件使用申请； 

上述三个一旦确定就不会变，但是也应该写到于环境有有关的文件中，如：在laravel中应该写到.env文件中；

另外还需要一个参数：

- token：由浙里办返回的ssotoken，返回参数如下：
    ```php
    [
        'sundata' => 'RU5DUllQVEVE4NW...5L4cdZ9/SA==', // string(644)
        'ssoticket' => 'AQIC5wM2LY4SfcwJq...UzEAAjAz*', // string(106) 
        'spappurl' => 'https://test.com/login',
        'ssotoken' => 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
        'reditct' => 'https://test.com/reditct',
    ]
    ```

### 1.2 控制器举例
控制器举例如下所示：
```php
namespace App\Http\Controllers;

use Anchu\Zhelb\Login\ZhelbLegalLogin;

class ApiController extends Controller
{
    /**
    * @param $params
    */
    public function ZhelbLegalLogin($params) {
        $url = '...';
        $ak = '...';
        $sk = '...';
        $token = $params['ssotoken'];
        $client = new ZhelbLegalLogin($url, $ak, $sk);
        $res = $client->getZhelbUserInfo($token);
        // todo: 下一步应该将用户信息存入到本地数据库，进行相关业务操作
//        $res = [
//            'errCode' => 0, // 0表示成功
//            'msg' => '错误说明',
//            'info' => [
//                'CompanyName' => '公司名称',
//                'uniscid' => '',
//                'CompanyRegNumber' => '',
//                'OrganizationNumber' => '',
//                'OrgType' => '',
//                'CompanyType' => '',
//                'CompanyAddress' => '',
//                'telephoneNumber' => '',
//                'CompanyScope' => '',
//                'CompanyRegOrg' => '',
//                'CompanySerialNumber' => '',
//                'username' => '',
//                'certName' => '',
//                'certNbxh' => '',
//                'certRegNo' => '',
//                'certHxbh' => '',
//                'certSn' => '',
//                'certDecSn' => '',
//                'certBase64' => '',
//                'loginType' => '',
//                'realLevel' => '',
//                'xzqh' => '',
//                'CompanyLegRep' => '',
//                'status' => '',
//                'attnPhone' => '',
//                'attnName' => '',
//                'attnIDType' => '',
//                'attnIDNo' => '',
//                'attnLandLinePhone' => '',
//            ]
//        ];
    }
}
```

### 1.3 如何获取token
假设上述控制器的路由为：
```shell
https://test.com/api/zhelib/login
```
因此可以采用如下方式调用浙里办接口来返回token（前提是已经请浙里办的技术支持添加了回调地址的白名单）：
```shell
http://essotest.zjzwfw.gov.cn/opensso/spsaehandler/metaAlias/sp?spappurl=https://test.com/api/zhelib/login&reditct=https://mapi.zjzwfw.gov.cn/web/mgop/gov-open/zj/2002281895/v0.0.1/index.html?debug=true
```
将上述地址输入浏览器，会调出浙里办的登录入口，输入账号密码后会返回token等信息。

### 1.3 资料
[浙里办政务服务网法人单点登陆对接指南](https://qianxiaoxia.yuque.com/docs/share/6ea3f302-0557-411b-be1f-e2bc6108da94?#)